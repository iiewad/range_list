# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.
class RangeList
  def initialize
    @range_hash = {}
    @range_flag = { 'START' => 'S', 'END' => 'E' }.freeze
  end

  def add(range)
    return if range.length != 2

    min = range.first
    max = range.last
    return if max < min || min == max

    @range_hash[min] = @range_flag['START']
    @range_hash[max] = @range_flag['END']
    rng_arr = @range_hash.keys.sort
    (1...rng_arr.length - 1).each do |i|
      left = i - 1
      left -= 1 while !@range_hash.key?(rng_arr[left]) && left.positive?
      pv = @range_hash[rng_arr[left]]

      right = i + 1
      right += 1 while !@range_hash.key?(rng_arr[right]) && right < rng_arr.length
      nv = @range_hash[rng_arr[right]]

      @range_hash.delete(rng_arr[i]) if pv == @range_flag['START'] && nv == @range_flag['END']
    end
  end

  def remove(range)
    return if range.length != 2

    min = range.first
    max = range.last
    return if max < min || min == max

    rng_arr = @range_hash.keys.sort
    rng_arr.each { |rng| @range_hash.delete(rng) if rng >= min && rng <= max }

    min_index = nil
    rng_arr = @range_hash.keys.sort
    rng_arr.each_with_index do |rng, i|
      min_index = i - 1 and break if rng >= min
    end

    if min_index.nil? && @range_hash[rng_arr.length - 1] == @range_flag['START']
      @range_hash[min] = @range_flag['END'] and return
    end

    pv = @range_hash[rng_arr[min_index]]
    @range_hash[min] = @range_flag['END'] if pv == @range_flag['START']

    max_index = nil
    rng_arr = @range_hash.keys.sort
    rng_arr.each_with_index { |rng, i| max_index = i and break if rng >= max }

    nv = @range_hash[rng_arr[max_index]]
    @range_hash[max] = @range_flag['START'] if nv == @range_flag['END']
  end

  def print
    op = []
    rng_arr = @range_hash.keys.sort
    (0...rng_arr.length).step(2).each do |i|
      op << "[#{rng_arr[i]}, #{rng_arr[i + 1]})"
    end

    puts op.join(', ')
  end
end

rl = RangeList.new
rl.add([1, 5])
rl.print
## Should display: [1, 5)

rl.add([10, 20])
rl.print
## Should display: [1, 5) [10, 20)

rl.add([20, 20])
rl.print
## Should display: [1, 5) [10, 20)

rl.add([20, 21])
rl.print
## Should display: [1, 5) [10, 21)

rl.add([2, 4])
rl.print
## Should display: [1, 5) [10, 21)

rl.add([3, 8])
rl.print
## Should display: [1, 8) [10, 21)

rl.remove([10, 10])
rl.print
## Should display: [1, 8) [10, 21)

rl.remove([10, 11])
rl.print
## Should display: [1, 8) [11, 21)

rl.remove([15, 17])
rl.print
## Should display: [1, 8) [11, 15) [17, 21)

rl.remove([3, 19])
rl.print
## Should display: [1, 3) [19, 21)
